import XCTest

import GSSAFirebaseManagerPackageTests

var tests = [XCTestCaseEntry]()
tests += GSSAFirebaseManagerPackageTests.allTests()
XCTMain(tests)
