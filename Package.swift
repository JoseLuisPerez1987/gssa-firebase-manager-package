// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "GSSAFirebaseManagerPackage",
    platforms: [
            .iOS(.v12)
        ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "GSSAFirebaseManagerPackage",
            targets: ["GSSAFirebaseManagerPackage","FirebaseABTesting","FirebaseAnalytics","FirebaseCore","FirebaseCoreDiagnostics","FirebaseCrashlytics","FirebaseDynamicLinks","FirebaseInstallations","FirebaseInstanceID","FirebaseMessaging","FirebasePerformance","FirebaseRemoteConfig","GoogleAppMeasurement","GoogleDataTransport","GoogleUtilities","nanopb","PromisesObjC","Protobuf"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "GSSAFirebaseManagerPackage",
            dependencies: []),
                .binaryTarget(name: "FirebaseABTesting", path: "FirebaseABTesting.xcframework"
                      ),
                .binaryTarget(name: "FirebaseAnalytics", path: "FirebaseAnalytics.xcframework"
                      ),
                .binaryTarget(name: "FirebaseCore", path: "FirebaseCore.xcframework"
                      ),
                .binaryTarget(name: "FirebaseCoreDiagnostics", path: "FirebaseCoreDiagnostics.xcframework"
                      ),
                .binaryTarget(name: "FirebaseCrashlytics", path: "FirebaseCrashlytics.xcframework"
                      ),
                .binaryTarget(name: "FirebaseDynamicLinks", path: "FirebaseDynamicLinks.xcframework"
                      ),
                .binaryTarget(name: "FirebaseInstallations", path: "FirebaseInstallations.xcframework"
                      ),
                .binaryTarget(name: "FirebaseInstanceID", path: "FirebaseInstanceID.xcframework"
                      ),
                .binaryTarget(name: "FirebaseMessaging", path: "FirebaseMessaging.xcframework"
                      ),
                .binaryTarget(name: "FirebasePerformance", path: "FirebasePerformance.xcframework"
                      ),
                .binaryTarget(name: "FirebaseRemoteConfig", path: "FirebaseRemoteConfig.xcframework"
                      ),
                .binaryTarget(name: "GoogleAppMeasurement", path: "GoogleAppMeasurement.xcframework"
                      ),
                .binaryTarget(name: "GoogleDataTransport", path: "GoogleDataTransport.xcframework"
                      ),
                .binaryTarget(name: "GoogleUtilities", path: "GoogleUtilities.xcframework"
                      ),
                .binaryTarget(name: "nanopb", path: "nanopb.xcframework"
                      ),
                .binaryTarget(name: "PromisesObjC", path: "PromisesObjC.xcframework"
                      ),
                .binaryTarget(name: "Protobuf", path: "Protobuf.xcframework"
                      ),
        .testTarget(
            name: "GSSAFirebaseManagerPackageTests",
            dependencies: ["GSSAFirebaseManagerPackage"]),
    ]
)
