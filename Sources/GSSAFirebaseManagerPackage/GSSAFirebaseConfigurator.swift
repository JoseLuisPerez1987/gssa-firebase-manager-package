//
//  File.swift
//  
//
//  Created by Jose Luis Perez Reyes on 06/05/21.
//

import Foundation
import FirebaseCore
public final class GSFirebaseConfigurator: NSObject {
    public func initFirebase(plistName:String,nameClass:AnyClass) -> Bool {
        let bundle = Bundle(for: type(of: self))
        if let plistPath = bundle.path(forResource: plistName, ofType: "plist") {
            guard let options = FirebaseOptions(contentsOfFile: plistPath) else {
                fatalError()
            }
            FirebaseApp.configure(options: options)
            print("FirebaseApp.configure Success")
            return true
        }else{
            print("FirebaseApp.configure Failed")
            return false
        }
    }

   public func initWithFileConfig(nameFileToInitFirebase:String,idBundleGSSAModule:String){
        let firebasePlistFileName = nameFileToInitFirebase
        if let path = Bundle.main.path(forResource: firebasePlistFileName, ofType: "plist") {
            if let firbaseOptions = FirebaseOptions(contentsOfFile: path) {
                FirebaseApp.configure(options: firbaseOptions)
                print("FirebaseApp.configure Success")
            }
        }else{
            print("FirebaseApp.configure Failed")

        }
    }
}
